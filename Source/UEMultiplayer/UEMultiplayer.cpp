// Copyright Epic Games, Inc. All Rights Reserved.

#include "UEMultiplayer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UEMultiplayer, "UEMultiplayer" );
 