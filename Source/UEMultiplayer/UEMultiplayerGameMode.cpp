// Copyright Epic Games, Inc. All Rights Reserved.

#include "UEMultiplayerGameMode.h"
#include "UEMultiplayerCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUEMultiplayerGameMode::AUEMultiplayerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AUEMultiplayerGameMode::HostLANGame()
{
	GetWorld()->ServerTravel("Game/ThirdPerson/Maps/ThirdPersonMap?Listen");
}

void AUEMultiplayerGameMode::JointLANGame()
{
	APlayerController * PC = GetGameInstance()->GetFirstLocalPlayerController();

	if(PC)
	{
		PC->ClientTravel("26.38.37.252", TRAVEL_Absolute);
	}
}
