// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBox.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AMyBox::AMyBox()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ReplicatedVar = 100.0f;
}

// Called when the game starts or when spawned
void AMyBox::BeginPlay()
{
	Super::BeginPlay();

	SetReplicates(true);
	SetReplicateMovement(true);

	StartPosition = GetActorLocation();

	if(HasAuthority())
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Start Timer"));
		GetWorld()->GetTimerManager().SetTimer(TestTimer, this, 
			&AMyBox::DecreaseReplicatedVar, 2.f, false);
	}
}

// Called every frame
void AMyBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*if(HasAuthority())
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Is Server"));
	}else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Is Client"));
	}*/
}

void AMyBox::Destroyed()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Black, TEXT("On Destroy"));
}




void AMyBox::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMyBox, ReplicatedVar);
}

void AMyBox::DecreaseReplicatedVar()
{
	if (HasAuthority())
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, FString::Printf( TEXT("Run Timer End : %d"), GPlayInEditorID));
		GetWorld()->GetTimerManager().SetTimer(TestTimer, this,
			&AMyBox::DecreaseReplicatedVar, 2.f, false);
	}
}

void AMyBox::OnRep_ReplicatedVar()
{
	if(HasAuthority())
	{
		const FVector NewLocation = StartPosition + FVector(0.f, 0.f, ReplicatedVar);
		SetActorLocation(NewLocation);
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Server : OnRep_RelicatedVar"));

	}else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Client %d: OnRep_ReplicatedVar"),GPlayInEditorID));
	}
}

