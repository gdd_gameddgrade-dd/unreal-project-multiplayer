// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UEMultiplayerGameMode.generated.h"

UCLASS(minimalapi)
class AUEMultiplayerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUEMultiplayerGameMode();

	UFUNCTION(BlueprintCallable)
	void HostLANGame();

	UFUNCTION(BlueprintCallable)
	void JointLANGame();
};



